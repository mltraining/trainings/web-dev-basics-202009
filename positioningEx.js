
function openNav() {
    document
        .querySelector('nav')
        .style.width = '100%';
    document
        .querySelector('#nav-button')
        .innerHTML = '<i class="material-icons">close</i>';
}

function closeNav() {
    document
        .querySelector('nav')
        .style.width = '40px';
    document
        .querySelector('#nav-button')
        .innerHTML = '<i class="material-icons">menu</i>';
}

let isNavOpen = false;

function toggleNav() {
    if (isNavOpen) {
        closeNav();
        
        isNavOpen = false;
    } else {
        openNav();
        isNavOpen = true;
    }
}