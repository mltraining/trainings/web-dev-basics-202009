function playMyVideo() {
    document.querySelector('video').play();
}

function pauseMyVideo() {
    document.querySelector('video').pause();
}

function volumeUp() {
    let currentVolume = document.querySelector('audio').volume;
    if (currentVolume > 0.9) {
        currentVolume = 0.1;
    } else {
        currentVolume = currentVolume + 0.1;
    }
    document.querySelector('audio').volume = currentVolume;
    console.log(currentVolume);
}
